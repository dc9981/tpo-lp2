# Dokument zahtev

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | Deni Cerovac, Jan Ivanović, Roni Likar, Luka Železnik |
| **Kraj in datum** | Ljubljana, 02.04.2019 |



## Povzetek projekta

Osrednji predmet tega dokumenta je opis posameznih vlog, funkcionalnosti, primerov uporabe ter prikaz osnutkov prikaznih mask. Na začetku predstavimo problemsko domeno in zapišemo kratek pregled pomembnejših funkcionalnosti, ki opisujejo delovanje aplikacije. Nato definiramo uporabniške vloge, kjer opredelimo glavne tipe uporabnikov aplikacije glede na funkcionalnosti, ki jih imajo na voljo. Ustvarimo tudi slovar pojmov, ki se uporabljajo skozi celoten dokument. Temu sledi prikaz diagrama primerov uporabe, kjer je predstavljena interakcija med akterji in funkcionalnostmi ter podrobnejši opis vsake od funkcionalnih zahtev, ki jih naša aplikacija podpira. Definiramo tudi omejitve, ki jih je potrebno upoštevati pri funkcionalnostih ali skozi celoten razvoj aplikacije. Prikažemo grobo idejo, kako naj bi izgledale zaslonske maske in njihove povezave s funkcionalnostmi. Na koncu pa še opišemo vmesnike do zunanjih sistemov, ki smo jih pri razvoju uporabili.

## 1. Uvod

Naša spletna stran bo olajšala organizacijo študentom. Študentje so vedno zelo zaposleni in nimajo veliko prostega časa, zato smo se odločili narediti spletno stran, ki bo pomagala študentom pri organizaciji dela za študij. Spletna stran bo bila dostopna tako neregistriranim kot registriranim študentom. Neregistrirani študentje bojo imeli le nekaj osnovnih funkcionalnosti na voljo. Registrirani študentje bojo imeli na volijo veliko več funkcionalnosti, zato bomo spodbujali študente da se registrirajo na našo spletno stran. Če se študent ne želi registrirati bo imel na voljo le okrnjen del funkcionalnosti spletne strani. Tako bo lahko vsak neregistriran uporabnik uporabljal le TODO listo, na katero bo lahko pisal opravila, ki jih rabi opraviti. TODO lista bo omogočala da na enostaven način naštejemo opravila, ki jih želimo narediti tekom dneva. Ko določeno opravilo opravimo, ga lahko odkljukamo. Vsa opravila ki smo jih odkljukali se nam bodo ob koncu dneva zbrisala. Tista pa, ki jih nismo uspeli narediti, se nam bodo prenesla v naslednji dan. Tako bo imel vsak študent na enem mestu lepo organizirano katera opravila ga še čakajo. Ta funkcionalnost bo seveda na voljo tudi registriranim študentom. Glavna prednost registriranih študentov je, da se lahko vpišejo v predmete. Predmete bojo ustvarili profesorji, ki bodo imeli dostop do spletne strani. Profesor bo za vsak predmet posebej lahko napisal kdaj se predmet izvaja in kateri so pomembni datumi pri posameznem predmetu. Tako bo imel profesor enostaven pregled nad vsemi predmeti, ki jih izvaja. Registrirani študentje bojo imeli na voljo tedenski urnik, kjer bo prikazano kateri predmeti se kdaj izvajajo. Lahko pa bo v urnik dodal tudi kakšne izven šolske dejavnosti. Poleg urnika pa bo registriranim študentom na voljo tudi koledar na katerem bodo označeni vsi pomembni datumi kolokvijev in izpitov ter ostalih obveznosti pri posameznem predmetu. Ko se bo študent vpisal v predmet se mu bo lahko avtomatsko posodobil urnik in koledar. V urniku se mu bo vpisalo kdaj se predmet izvaja, v koledar pa vsi pomembni datumi predmeta. Če bo profesor spremenil izvajanje predmeta, bodo spremembe vidne vsem študentom, ki so vpisani v predmet. Tudi ko bo profesor dodal ali spremenil kakšno obveznost pri predmetu, se bo vsem študentom, ki so vpisani na predmet posodobil koledar. Tako bomo zagotovili, da so vedno vsi na tekočem z vsemi spremembami. Študentje si bodo lahko lažje organizirali dneve za učenje pred izpiti in profesorji bodo lahko na enostaven način seznanjali študente z sprotnimi obveznostmi. Vsak registriran študent bo imel redovalnico, v katero bo lahko pisal ocene, ki jih je dobil pri predmetih. To bo v veliko pomoč študentom, saj si ne bojo rabili zapomnit na pamet katere ocene so dobili pri katerih predmetih. Pri vsakem predmetu, si bo lahko študent izbral še ciljno oceno, torej tisto oceno s katero ocenjuje da bo končal predmet. S tem bo imel študent merljive cilje, ki mu bodo pomagali pri opravljanju predmeta. Ker lahko vsak profesor izvaja več predmetov, omogočamo profesorjem da na enostaven način dodajo nove predmete. Vsak predmet bo imel določen naziv, čas izvajanja in seznam pomembnih datumov. Ko se bo študent vpisal v predmet bo imel opcijo, da si izbere ali se mu posodobi le urnik ali samo koledar, oboje ali pa nič. Profesorji se bodo lahko registrirali v sistem na enak način kot študentje. Potrebno bo izpolniti ime, priimek, datum rojstva, spol, fakulteto, mail in geslo. Študentje bojo imeli takojšen dostop do spletne strani ob registraciji. Profesorje pa bo ročno potrdil administrator. Tako bomo zagotovili legitimnost vsakega profesorja. Spletna stran bo namenjena predvsem študentom, zato profesorji ne bojo imeli na voljo toliko funkcionalnosti kot študentje. Njihova glavna funkcionalnost bo dodajanje predmetov in njihovo urejanje. Administrator sistema pa bo imel neomejen dostop do podatkovne baze in sistema. Njegova naloga bo dodajanje novih profesorjev v sistem in reševanje težav na strani sistema. V primeru, da bo prišlo do težav pri dodajanju predmeta v urnik ali dogodkov v koledar, bo imel administrator možnost to storiti ročno. Tako bo lahko administrator skrbel da sistem deluje in ko bo prišlo do napak bo lahko te napake sporočil programerjem, da jih odpravijo.


## 2. Uporabniške vloge

**Glavni tipi uporabnikov aplikacije glede na funkcionalnosti, ki jih imajo na voljo:**

1. **Neregistriran uporabnik:** Ima možnost uporabe TO-DO liste, kjer lahko doda ali zaključi opomnik. V aplikacijo se lahko tudi registrira.
2. **Študent:** Oseba, ki je vpisana v fakulteto. Ima možnost uporabe koledarja, TO-DO liste, urnika in redovalnice ter vseh funkcionalosti, ki so jim dopisane. Poleg tega se lahko vpiše k določenemu predmetu.
3. **Profesor:** Oseba, ki je zaposlena kot predavatelj na fakulteti. Njegova glavna naloga je dodajanje predmetov v sistem. Vsakemu predmetu lahko določi urnik in obveznosti, ki mu pripadajo. Te lahko ob potrebi tudi uredi.
4. **Administrator:** Pregleda prošnje registracij profesorjev ter preveri njihovo legitimnost. V primeru, da je ta primerna jih doda v sistem.


## 3. Slovar pojmov
* Uporabnik - oseba, ponavadi študent ali profesor, ki uporablja našo aplikacijo
* Fakulteta - ustanova v kateri študentje pridobivajo znanje določenega področja. Zaposluje profesorje, kateri predavajo in ocenjujejo znanja študentov.
* Uporabniški vmesnik - prikaz spletne strani uporabniku, s katero lahko uporabnik komunicira s klikom miške po določenih elementih.
* Aplikacija - spletna stran, s katero upravljajo uporabniki.
* Račun - uporabniku omogoča dostop do aplikacije. Vsak uporabnik ima lahko z enim mailom le en račun
* Sistem - vse funkcionalnosti, ki jih aplikacija omogoča
* Google Calendar API - zunanji vmesnik, ki omogoča shranjevanje datumov v Googlov koledar
* Redovalnica - funkcionalnost, ki omogoča študentu shranjevanje ocen, ki jih je dobil pri posameznih predmetih
* Ocena - vrednost v celoštevilskem intervalu od 1 do 10. 
* Predmet - snovi, ki je zaokrožena v zaključeno celoto in obsega specifično področje smeri, ki jo študent študira. Za vsak predmet je zadolžen določen profesor, ki predmet tudi poučuje.
* Opomnik - funkcionalnost, ki omogoča študentu shranjevanje pomembnih datumov v koledar. Uporabnika opozori ob pomembnih datumih.
* Urnik - funkcionalnost, ki študentom prikazuje tedenski razpored predmetov po določenih urah
* Koledar - funkcionalnost, ki študentom omogoča prikaz dejavnosti po dnevih. Prikazuje dejavnosti v določenem tednu ali mesecu.
* TO-DO - funkcionalnost, ki omogoča neregistriranim uporabnikom in študentom, da zapišejo seznam opravil, ki jih želijo opraviti tekom dneva. 
* Aktivnost - dejavnost, ki jo študent lahko doda v urnik.
* Obveznost - pomemben dogodek, ki si ga želimo zapomniti, zato ga dodamo v koledar



## 4. Diagram primerov uporabe

* Diagram predstavlja interakcijo med akterji in funkcionalnostmi. 
* Akterji so uporabniške vloge in zunanja komponenta, ki komunicirajo z našo aplikacijo.
* Funkcionalnosti, ki jih naš sistem ponuja, so prikazane v osrednjem delu diagrama. 
* Uporabniške vloge so prikazane na levi, zunanji sistemi pa na desni strani diagrama.

![Diagram primera uporabe](../img/Diagram_Primera_Uporabe.PNG)


## 5. Funkcionalne zahteve

### 1. Registracija


#### Povzetek funkcionalnosti

* Neregistriran uporabnik se lahko registrira v sistem preko uporabniškega vmesnika. Vmesnik bo vseboval polja za vnos imena, priimka, fakultete, starosti, spola, maila in gesla. Pri registraciji bo lahko neregistriran uporabnik izbral registracijo vrste Študent ali Profesor. Registracija vrste Profesor potrebuje potrdilo administratorja.


#### Osnovni tok

Osnovni tok za uporabniško vlogo neregistriranega uporabnika, ki se želi prijaviti kot Študent:

1. Neregistriran uporabnik klikne na gumb registracija
2. Odpre se uporabniški vmesnik za registracijo
3. Neregistriran uporabnik vpiše ime, priimek, starost, spol, fakulteto, mail in geslo, ter pritisne na gumb Študent
4. Sistem shrani neregistriranega uporabnika v bazo kot Študenta
5. Odpre se glavna stran aplikacije

Osnovni tok za uporabniško vlogo neregistriranega uporabnika, ki se želi prijaviti kot Profesor:

1. Neregistriran uporabnik klikne na gumb registracija
2. Odpre se spletna stran za registracijo
3. Neregistriran uporabnik vpiše ime, priimek, starost, spol, fakulteto, mail in geslo, ter pritisne na gumb Profesor
4. Ob uspešni registraciji neregistriranega uporabnika, se profesorju prikaže obvestilo
5. Neregistriran uporabnik počaka na potrditev s strani administratorja
6. Ko ga administrator potrdi sistem shrani neregistriranega uporabnika v bazo kot Profesorja
7. Profesor dobi mail o uspešni potrditvi

#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Sistem za registracijo uporabnikov ne deluje in se uporabniku prikaže obvestilo o napaki na strani sistema.
* Neregistriran uporabink ne izpolne vseh potrenih polj, zato mu sistem javi mu napako.
* Neregistriran uporabniku se ne ujema napisano geslo v enemu izmed polj
* Neregistriran uporabink vpiše email, ki je že shranjen v sistem, sistem mu javi napako.

#### Pogoji
* Funckionalnost Registracija je namenjena le neregistriranim uporabnikom. Ponovna uporaba emaila, ki je že obstaja v sistemu je prepovedana. Na en mail je lahko vezan le en račun.
* Pri funkcionalnosti Registracija, študent ne sme imeti že obstoječega računa v sistemu. Na en mail je lahko vezan le en račun.


#### Posledice
* Ob uspešni Registraciji je neregistrairan uporabink shranjen v bazo, kot Študent ali Profesor. Vsak od njiju ima na voljo svoje tipe funkcionalnosti. 

#### Posebnosti
* Potrebno je narediti varen registracijski sistem. V bazo je potrebno shranjevati zgoščena gesla.


#### Prioritete identificiranih funkcionalnosti
* Must have


#### Sprejemni testi
    
1. Testiranje funkcionalnosti za registracijo študenta:
	* Uporabnik je neregistriran na registracijskem zaslonu
	* Izpolnemo vsa polja, ki so potrebna za registracijo in se s klikom na gumb Študent registriramo v sistem. Ob uspešni registraciji se prikaže glavna stren aplikacije.
	* Študent je uspešno registriran
2. Testiranje funkcionalnosti za registracijo profesorja:
	* Uporabnik je neregistriran na registracijskem zaslonu
	* Izpolnemo vsa polja, ki so potrebna za registracijo in s klikom na gumb Profesor pošljemo prošnjo administratorju. Ob administratorjevi potrditvi, smo uspešno registrirani v sistem.
	* Profesor je uspešno registriran

-------------

###2. Prijava

#### Povzetek funkcionalnosti
* Študent, profesor in administrator se lahko prijavijo v sistem preko uporabniškega vmesnika. Vmesnik bo vseboval polje za vnos maila in gesla. 


#### Osnovni tok

Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb Prijava
2. Odpre se uporabniški vmesnik za prijavo
3. Študent vpiše mail in geslo ter pritisne na gumb Prijava
4. Sistem avtenticira študenta 
5. Študentu se odpre glavna stran za njegov tip vloge

Osnovni tok za uporabniško vlogo Profesor:

1. Profesor klikne na gumb Prijava
2. Odpre se uporabniški vmesnik za prijavo
3. Profesor vpiše mail in geslo ter pritisne na gumb Prijava
4. Sistem avtenticira profesorja 
5. Profesorju se odpre glavna stran za njegov tip vloge

Osnovni tok za uporabniško vlogo Administrator:

1. Administrator klikne na gumb Prijava
2. Odpre se uporabniški vmesnik za prijavo
3. Administrator vpiše mail in geslo ter pritisne na gumb Prijava
4. Sistem avtenticira administratorja 
5. Administratorju se odpre glavna stran za njegov tip vloge


#### Izjemni tokovi
* Študent, profesor ali administrator vpiše napačen mail. Sistem ga ne spusti na glavno stran in mu prikaže obvestilo o napačnem mailu.
* Študent, profesor ali administrator vpiše napačeno geslo. Sistem ga ne spusti na glavno stran in mu prikaže obvestilo o napačnem geslu.
* Sistem za avtentikacijo ne deluje in zato se prikaže obvestilo, da prijava trenutno ni možna.

#### Pogoji
* Funkcionalnost prijava je namenjena le registriranim uporabinkom, torej Študentu, Profesorju ali Administratorju. Neregistriranemu uporabniku funkcionalnost ni na voljo.

#### Posledice
* Ob uspešni prijavi v sistem ima uporabnik na voljo več funkcijonalnosti, te pa so odvisne od tipa uporabniške vloge pri prijavi. 


#### Posebnosti
* Potrebno je narediti varen avtentikacijski sistem. V bazo je potrebno shranjevati zgoščena gesla.


#### Prioritete identificiranih funkcionalnosti
* Must have


#### Sprejemni testi
	
1. Testiranje funkcionalnosti za prijavo študenta:
	* Smo na prijavnem zaslonu in imamo račun študenta
	* V polja prijave vpišemo mail in geslo. Nato kliknemo gumb prijava. 
	* Prijavljeni smo v sistem kot študent
2. Testiranje funkcionalnosti za prijavo profesorja:
	* Smo na prijavnem zaslonu in imamo račun profesorja
	* V polja prijave vpišemo mail in geslo. Nato kliknemo gumb prijava.
	* Prijavljeni smo v sistem kot profesor
2. Testiranje funkcionalnosti za prijavo administratorja:
	* Smo na prijavnem zaslonu administratorja in imamo račun administratorja.
	* V polja prijave vpišemo mail in geslo. Nato kliknemo gumb prijava.
	* Prijavljeni smo v sistem kot administratorja.
	
-------------

###3. Dodaj obveznost v koledar 


#### Povzetek funkcionalnosti
* Študent in profesor lahko dodata obveznost v koledar. Študent doda novo obveznost na koledar, ki se mu prikaže na novo po uspešno dodani obveznosti. Profesor ima opcijo dodat obveznost v povezavi z nekim predmetom. V tem primeru bo obveznost dodana v koledar vsem študentom, ki so vpisani v predmet. 


#### Osnovni tok

Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb Dodaj obveznost
2. Prikaže se mu okno, kjer lahko vpiše naziv, čas, datum, lokacijo in opis dogodka
3. Študent pritisne na gumb Ustvari in obveznost se bo dodala v koledar


##### Alternativni tok 1
1. Profesor klikne na gumb za dodat obveznost
2. Prikaže se mu linija, kjer lahko vpiše naziv in datum
3. Profesor s pritiskom gumba Enter na tipkovnici ali s pritiskom miške izven območja linije shrani na novo ustvarjeno obveznost
4. Obveznost je dodana v koledar vsem študentom, ki so v predmet vpisani. Pri tem so tudi obveščeni

#### Izjemni tokovi
* Če študent ali profesor ne izpolneta zaželjena polja jima sistem javi napako in obveznosti ni shranjena

#### Pogoji
* Funkcionalnost Dodaj obveznost v koledar je namenjena le uporabniškima vlogama Študent in Profesor. Uporabniškima vlogama Neregistriran uporabnik ter Administrator funkcionalnost ni na voljo.

#### Posledice
* V primeru, da je bila obveznost uspešno dodana, se nam ta prikaže v koledarju. 


#### Posebnosti
* Potrebno je implementirati Google Calendar API, za shranjevanje dogodkov v koledar.


#### Prioritete identificiranih funkcionalnosti
* Must have


#### Sprejemni testi 
1. Testiranje funkcionalnosti Dodaje obveznosti v koledar kot študent:
	* V sistemu smo prijavljeni kot študent in se nahajamo na zaslonu koledarja.
	* Pritisnemo na gumb za dodajanje obveznosti v koledar. Nato izpolnemo vsa polja in pritisnemo na gumb potrdi. 
	* Obveznosti se doda v koledar.
2. Testiranje funkcionalnosti Dodaje obveznosti v koledar kot profesor:
	* V sistemu smo prijavljeni kot profesor in se nahajamo na zaslonu s seznamom predmetov.
	* Izberemo predmet in mu dodamo obveznost. Izpolnemo vsa polja obveznosti in shranimo.
	* Vsem študentom, ki so vpisani v predmet se obveznost prikaže na koledarju.

-------------

###4. Uredi/izbriši obveznost v koledarju

#### Povzetek funkcionalnosti
* Študent in profesor lahko uredijo/odstranijo obveznost iz koledarja. Profesor ima opcijo odstraniti ali urediti obveznost v povezavi z nekim predmetom. V tem primeru bo obveznost uspešno izbrisana ali urejena iz koledarja vsem študentom, ki so vpisani v predmet.  


#### Osnovni tok

Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb Koledar, kjer se mu odpre tedenski pogled koledarja
2. Študent klikne na obveznost, ki jo želi urediti/izbrisati
3. Prikaže se mu pojavno okno, kjer lahko spreminja naziv, čas, datum, lokacijo in opis dogodka
4. Če uporabnik pritisne na gumb za Izbriši, se bo obveznost po dodatni potrditvi izbrisala. V primeru, da klikne na gumb shrani, pa se bodo spremembe shranile

#### Alternativni tok 1
1. Profesor klikne na gumb Predmeti in izbere predmet ki ga želi urediti/izbrisati
2. Profesor ima opcijo spremeniti naziv in datum obveznosti tega predmeta
3. Če profesor pritisne na gumb za  Izbriši, se bo obveznost izbrisala. V primeru, da klikne gumb Enter na tipkovnici ali pritiskom miške izven območja linije se spremembe obveznosti shranijo
4. Študentom, ki so vpisani na predmet se obveznost spremeni ali izbriše iz koledarja. O tem so seveda tudi obveščeni.

#### Izjemni tokovi
* Študent ali profesor izbriše vsebino polja in pritisne na gumb Uredi. Sistem mu javi napako, da morajo biti usa polja izpolnjena.

#### Pogoji
* Funkcionalnost Uredi/odstrani dogodek iz koledarja je namenjena le uporabniškima vlogama Študent in Profesor. Uporabniškima vlogama Administrator in Neregistriran uporabnik funkcionalnost ni na voljo.

#### Posledice
* V primeru, da smo obveznost uspešno odstranili, v koledarju ne bo več vidna. Če smo obveznost uspešno uredili, pa bojo spremembe vidne v koledarju. 

#### Posebnosti
* Potrebno je implementirati Google Calendar API, za urejanje in brisanje dogodkov v koledarju.

#### Prioritete identificiranih funkcionalnosti
* Must have

#### Sprejemni testi 
1. Testiranje funkcionalnosti Urejanja obveznosti v koledar kot študent:
	* V sistemu smo prijavljeni kot študent in se nahajamo na zaslonu koledarja.
	* Kliknemo na obveznost, ki jo želimo spremeniti. Odpre se nam okno, v katerem lahko spreminjamo naziv in datum obveznosti. Nato kliknemo na gumb Shrani. 
	* Obveznosti se posodobi v koledarju.

2. Testiranje funkcionalnosti Dodaje obveznosti v koledar kot profesor:
	* V sistemu smo prijavljeni kot profesor in se nahajamo na zaslonu s seznamom predmetov.
	* Izberemo predmet in spremenimo datum obstoječe obveznosti. Nato kliknemo na gumb shrani.
	* Vsem študentom, ki so vpisani v predmet se bo obveznost posodobila na koledarju.

-------------

###5. Dodaj predmet v seznam


#### Povzetek funkcionalnosti
Profesor lahko ustvari predmet. Za njega lahko določi različne parametre kot so naziv, opis, pomembni datum, čas izvajanja.

#### Osnovni tok

1.  Profesor klikne na gumb Predmeti, odpre se mu seznam ustvarjenih predmetov.
2.	Profesor klikne na gumb Dodaj predmet.
3.	Odpre se uporabniški vmesnik, kjer lahko ustvari nov predmet.
4.	Profesor vpiše ime predmeta, krajši opis, poimenuje in določi pomembne datume obveznosti ter določi ure izvajanja pedmeta z dnevom.
5.	Profesor klikne na gumb Ustvari.
6.	Na novo ustvarjen predmet se zapiše v bazo in doda na listo predmetov profesorja.


#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Če profesor ne izpolni zaželjena polja mu sistem javi napako in predmet ni shranjen.

#### Pogoji
* Funkcionalnost Dodaj predmet je namenjena le uporabniški vlogi Profesor. Uporabniškim vlogam Neregistriran uporabnik, Študent in Administrator funckionalnost ni na voljo.
* Naziv predmeta, ki ga profesor napiše ne sme že obstajati v bazi aplikacije.

#### Posledice
* Ob uspešnemu dodajanju predmeta, se le ta prikaže profesorju na seznamu predmetov. Poleg tega je predmet na voljo uporabniški vlogi Študent in se lahko na njega vpišejo.

#### Posebnosti
* Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti
* Must have


#### Sprejemni testi 
    
1. Testiranje funkcionalnosti za dodat predmet v seznam:
	* v aplikacijo smo prijavljeni kot profesor, nahajamo se na zaslonu kjer je viden seznam predmetov
	* izberemo gumb Dodaj predmet, prikaže se nam prikazno okno, ki omogoča dodajanje predmeta. Izpolnemo vsa polja, ki so potrebna za dodat predmet in s klikom na gumb Ustvari dodamo nov predmet
	* predmet je dodan v seznam predmetov 
	
-------------

###6. Uredi/izbriši predmet v seznamu

#### Povzetek funkcionalnosti
* Profesor lahko uredi/izbriše svoj predmet.

#### Osnovni tok
1.  Profesor klikne na gumb Predmeti, odpre se mu seznam ustvarjenih predmetov.
1.	Profesor klikne na gumb Uredi.
2.	Odpre se uporabniški vmesnik, ki omogoča urejanje izbranega predmeta.
3.	Profesor lahko spremeni ime predmeta, krajši opis, pomembne datume obveznosti ter ure izvajanja pedmeta z dnevom, nato pa klikne gumb Uredi. Če namesto tega klikne gumb Izbriši, se predmet ob dodatnem potrdilu profesorja izbriše.
4.	V primeru urejanja, je predmet na seznamu prikazan s spremembami, če je Profesor predmet izbirsal, je predmet odstranjen iz seznama.

#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Če profesor ne izpolni zaželjena polja mu sistem javi napako in predmet ni posodobljen.

#### Pogoji
* Funkcionalnost Uredi/izbriši predmet je namenjena le uporabniški vlogi Profesor. Uporabniškim vlogam Neregistriran uporabnik, Študent in Administrator funckionalnost ni na voljo.

#### Posledice
* Če se predmet uspešno uredi, se spremenjen shrani v bazo in se posodobi. Posodobijo se tudi urniki in/ali koledarji vseh Študentov, ki so v predmet vpisani, glede spremembe pa so obveščeni. Če profesor izbriše predmet se ta odstrani iz njegovega seznama predmetov ter  iz koledarjev/urnikov študentov, ki so v predmet vpisani.

#### Posebnosti
* Ni posebnosti.


#### Prioritete identificiranih funkcionalnosti
* Must have


#### Sprejemni testi 

1. Testiranje funkcionalnosti za uredit predmet v seznamu:
	* v aplikacijo smo prijavljeni kot profesor, nahajamo se na zaslonu kjer je viden seznam predmetov
	* izberemo gumb Uredi, prikaže se nam prikazno okno, ki omogoča uredjanje predmeta. Izpolnemo vsa polja, ki jih želimo urediti in s klikom na gumb Uredi uredimo predmet.
	* predmet in njegove spremembe so vidne v seznamu predmetov

2. Testiranje funkcionalnosti za izbrisat predmet v seznamu:
	* v aplikacijo smo prijavljeni kot profesor, nahajamo se na zaslonu kjer je viden seznam predmetov
	* izberemo gumb Uredi predmet, prikaže se nam prikazno okno, ki omogoča urejanje predmeta. Izberemo gumb Izbriši, pojavi se okno kjer potrdimo brisanje
	* predmet se izbriše in ni več prikazan v seznamu predmetov

-------------
###7. Dodaj opomnik na TO-DO 

#### Povzetek funkcionalnosti

* Neregistriran uporabnik, študent lahko dodaja opomnike v svoj TO-DO list preko uporabniškega vmesnika. Vmesnik bo vseboval polje za napisat opomnik in gumb za potrditev napisangea.


#### Osnovni tok

Osnovni tok za uporabniško vlogo neregistriranega uporabnika, ki želi dodati opomnik v TO-DO:

1. neregistriran uporabnik izbere prikaz za TO-DO list.
2. sistem se prikažejo njegovi opomniki na TO-DO listu.
3. neregistriran uporabnik izbere gumb dodaj opomnik.
4. sistem se prokaže uporabniški vmesnik za izpolnit podatke o opomniku.
5. neregistriran uporabnik izpolne podatke opomnika.
6. neregistriran uporabnik potrdi dokončan opomnik.

Osnovni tok za uporabniško vlogo študenta, ki želi dodati opomnik v TO-DO:

1. študent izbere prikaz za TO-DO list.
2. sistem se prikažejo njegovi opomniki na TO-DO listu.
3. študent izbere gumb dodaj opomnik.
4. sistem se prokaže uporabniški vmesnik za izpolnit podatke o opomniku.
5. študent izpolne podatke opomnika.
6. študent potrdi dokončan opomnik.

#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Študent ne more dodati opomnika zaradi prevelkega števila znakov.
* Neregistriran uporabnik ne more dodati opomnika zaradi prevelikega števila znakov

#### Pogoji
* Funkcionalnost Dodaj opomnik na TO-DO je na voljo uporabniškim vlogam Študent in Neregistriran uporabnik. Uporabniškim vlogam Profesor in Administator funkcionalnost ni na voljo. 
* Besedilo opomnika mora biti krajše od omejitve (100 znakov).


#### Posledice
* Če uporabnik uspešno doda opomnik, bo ta dodan v TO-DO list.

#### Posebnosti
* Ni posebnisti


#### Prioritete identificiranih funkcionalnosti
* should have


#### Sprejemni testi

1. Testiranje funkcionalnosti za dodat opomnik na TO-DO kot študent:
	* v aplikacijo smo prijavljeni kot študent, nahajamo se na zaslonu kjer je viden TO-DO list
	* izberemo gumb za dodat opomnik na listo, prikaže se nam prikazno okno, ki omogoča dodajanje opomnika. Napišemo željen opomnik v polje in s klikom na gumb Dodaj
	* opomnik je uspešno dodan in viden na TO-DO listi.
	
2. Testiranje funkcionalnosti za dodat opomnik na TO-DO kot neregistriran uporabnik:
	* aplikacijo uporabljamo kot neregistriran uporabnik, nahajamo se na zaslonu kjer je viden TO-DO list
	* izberemo gumb za dodat opomnik na listo, prikaže se nam prikazno okno, ki omogoča dodajanje opomnika. Napišemo željen opomnik v polje in s klikom na gumb Dodaj
	* opomnik je uspešno dodan in viden na TO-DO listi.
    
-------------

###8. Zaključi opomnik na TO-DO  

#### Povzetek funkcionalnosti

* Neregistriran uporabnik, študent lahko zaključi opomnike preko uporabniškega vmesnika. Vmesnik vsebuje check-box zraven vsakega opomnika s čimer ga lahko zaključimo.

#### Osnovni tok

Osnovni tok za uporabniško vlogo Neregistriranega uporabnika, ki želi zaključiti opomnik v TO-DO:

1. neregistriran uporabnik izbere prikaz za TO-DO list.
2. sistem prikaže opomnike na TO-DO listu.
3. neregistriran uporabnik izbere enega ali več opomnikov tako da klikne na krogec sive barve zrven opomnika.
4. neregistriranemu uporabniku se označeni opomniki prikažejo koz zaključeni, tako da se krogec zmanjša in prikaže v zeleni barvi.

Osnovni tok za uporabniško vlogo Študent, ki želi zaključiti opomnik v TO-DO:

1. študent izbere prikaz za TO-DO list.
2. sistem prikaže opomnike na TO-DO listu.
3. študent izbere enega ali več opomnikov tako da klikne na krogec sive barve zrven opomnika.
4. štidentuu se označeni opomniki prikažejo koz zaključeni, tako da se krogec zmanjša in prikaže v zeleni barvi.

            
#### Alternativni tok(ovi)

* Ni alternativnih tokov

#### Izjemni tokovi
* Ni izjemnih tokov

#### Pogoji
* Funkcionalnost Dodaj opomnik na TO-DO je na voljo uporabniškim vlogam Študent in Neregistriran uporabnik. Uporabniškim vlogam Profesor in Administator funkcionalnost ni na voljo. 

#### Posledice
* Izbrani opomniki bodo še vedno vidni ampak označeni kot zaključeni/dokončani. Zaključeni opomniki se ob začetku novega dneva izbrišejo iz lista

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* should have

#### Sprejemni testi

1. Testiranje funkcionalnosti za zaključit opomnik na TO-DO listi kot študent:
	* v aplikacijo smo prijavljeni kot študent, nahajamo se na zaslonu kjer je viden TO-DO list
	* kliknemo na okrogel siv gumb, ki se nahaja desno od opomnika, ki ga želimo zaključiti. Gumb in velikost črk opomnika se zmanjšata, barva gumba se spremeni v zeleno, barva opomnika pa v svetlo sivo (manj vidno)
	* opomnik je uspešno zaključen
	
2. Testiranje funkcionalnosti za zaključit opomnik na TO-DO listi kot neregistriran uporabnik:
	* aplikacijo uporabljamo kot neregistriran uporabnik, nahajamo se na zaslonu kjer je viden TO-DO list
	* kliknemo na okrogel siv gumb, ki se nahaja desno od opomnika, ki ga želimo zaključiti. Gumb in velikost črk opomnika se zmanjšata, barva gumba se spremeni v zeleno, barva opomnika pa v svetlo sivo (manj vidno)
	* opomnik je uspešno zaključen
	
-------------

###9. Dodaj aktivnost v urnik

#### Povzetek funkcionalnosti
* Študenti lahko dodajajo aktivnosti v urnik.

#### Osnovni tok


Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb Urnik
2. Prikaže se mu zaslon tedenskega urnika, kjer lahko dodaja aktivnosti
3. Ob kliku na gumb Dodaj aktivnost, se študentu prikaže pojavno okno v katerega napiše ime aktivnosti in dan ter uro začetka in konca aktivnosti.
4. Študent z klikom na gumb Potrdi, shrani na novo ustvarjeno aktivnost v sistem in aktivnost se mu prikaže na urniku.


#### Alternativni tok 1
1. Študent klikne na gumb aktivnost
2. Prikažejo se mu lista fakultet, med katerimi študent izbere eno
3. Po izbrani fakulteti se mu prikažejo na izbiro letniki, študent klikne zaželenega
4. Študentu se prikažejo aktivnosti tega letnika
5. Študent najde aktivnost, ki ga zanima in klikne na gumb Vpiši Me
6. S tem se mu v njegov urnik doda čas izvajanja aktivnosti


#### Izjemni tokovi
* V primeru da študent ne izpolne zahtevanih polj, mu sistem javi napako
* V primeru, da sistem ne dela se bo uporabniku prikazalo obvestilo.

#### Pogoji
* Funckionalnost Dodaj aktivnost v urnik je namenjena le uporabniški vlogi Študent, ki je prijavljen v sistem. Uporabniškim vlogam Profesor, Neregistriran Uporabnik in Administrator funkcionalnost ni na vojo.


#### Posledice
* V primeru, da smo aktivnost uspešno dodali, se nam bo le ta prikazal v urniku. 

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* Must have

#### Sprejemni testi 

1. Testiranje funkcionalnosti za dodajanje aktivnosti v urnik kot študent:
	* uporabnik je registriran kot študent na zaslonu ki prikazuje urnik
	* izpolnemo vsa polja ki so potrebna za ustvariti aktivnosti in s klikom na gumb ustvari potrdimo aktivnosti.
	* aktivnosti se nato doda v urnik.
2. Testiranje funkcionalnosti za dodajanje aktivnosti v urnik preko prijave na predmet kot študent:
	* uporabnik je registriran kot študent na zaslonu ki prikazuje predmete
	* kot študent lahko po meniju navigiramo do aktivnosti ter se na njo vpišemo.
	* aktivnost se doda v študentov urnik.

-------------

###10. Uredi/izbriši aktivnost v urniku

#### Povzetek funkcionalnosti
* Študenti lahko uredijo/izbrišejo posamezne aktivnost v urniku. V primeru, da profesor spremeni začetni in končni čas predmeta, se bo vsem študentom, ki so vpisani v predmet, ta posodobila.

#### Osnovni tok

Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb Urnik
2. Prikaže se mu zaslon tedenskega urnika, kjer lahko dodaja in ureja aktivnosti
4. Ob kliku na aktivnost, ki ga želi uredit ali izbrisati, se uporabniku prikaže pojavno okno v katerem lahko ureja podatke aktivnosti
5. Če študent klikne na gumb Izbriši in potrdi brisanje se aktivnost izbriše. Če študent uredi podatke aktivnosti in klikne na gumb Uredi se njegove spremembe shranijo v sistem in v urniku.


#### Alternativni tok 1
1. Profesor klikne  na gumb Predmeti, prikažejo se mu vsi spredmeti, ki jih je ustvaril
2. Profesor klikne na gumb Uredi za izbrani predmet, ki ga želi urediti/izbrisati
3. Profesor ima opcijo spremeniti naziv, čas izvajanja, opis predmeta, lokacijo in pomembne datume predmeta
4. Če profesor klikne na gumb Izbriši in potrdi brisanje, se predmet izbriše. Profesorse lahko odloči spremeniti naziv ali čas izvajanja predmeta in klikne na gumb Uredi. 
5. Na podlagi izbrane akcije se vsem študentom, ki so vpisani v predmet, le ta spremeni ali izbriše iz njihovega urnika. Glede spremembe so tudi obveščeni

#### Izjemni tokovi
* Študent izbriše vsebino polja in pritisne na gumb Uredi. Sistem mu javi napako, da morajo biti usa polja izpolnjena.
* Profesor izbriše vsebino polja in pritisne na gumb Uredi. Sistem mu javi napako, da morajo biti usa polja izpolnjena.

#### Pogoji
* Funckionalnost Uredi/izbriši aktivnost je na voljo le uporabniški vlogi Študent. Uporabniškim vlogam Administrator, Profesor in Neregistriran uporabnik ta funkcionalnost ni na voljo
* Študent lahko ureja samo predmete, ki jih je sam ustvaril

#### Posledice
* Če študent uspešno uredij ali odstrani predmet, se jim le ta spremeni ali izbriše iz urnika. Če profesor uspešno uredi ali odstrani predmet, se mu ta spremeni ali izbriše iz liste predmetov. Poleg tega, se predmet izbriše ali uredi tudi vsem študentom, ki so vpisani v predmet.

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* Must have

#### Sprejemni testi 

1. Testiranje funkcionalnosti za urejanje aktivnosti v urniku kot študent:
	* uporabnik je registriran kot študent na zaslonu ki prikazuje urnik.
	* izberemo poljubno aktivnost ter uredimo željena polja kar nato potrdimo s klikom na gumb ustvari.
	* aktivnosti se nato posodobi v urniku.
2. Testiranje funkcionalnosti za urejanje/brisanje predmetov v urniku kot profesor:
	* uporabnik je registriran kot profesor na strani ki mu prikazuje seznam predmetov.
	* izberemo predmet, ki ga želimo urediti ali izbrisati. V primeru urejanja uredimo poljubna polja in potrdimo s klikom na gumb uredi.
	* Spremembe se nam posodobijo, polega tega pa se spremembe psodobijo tudi vsem vpisanim študentom.
	
3. Testiranje funkcionalnosti za brisanje predmetov v urniku kot študent:
	* uporabnik je registriran kot študent na zaslonu ki prikazuje urnik
	* izberemo željeno aktivnost ter jo s klikom na gumb izbriši odstranimo iz našega urnika.
	* aktivnost je sedaj odstranjena iz našega urnika
	
------------------

###11. Odjava

#### Povzetek funkcionalnosti
* Študent, profesor ali administrator se lahko odjavijo iz spletne aplikacije. S tem izgubijo možnost uporabe funckionalnosti, katere so jim bile pred tem na voljo.

#### Osnovni tok

Osnovni tok za uporabniško vlogo Študent:

1. Študent klikne na gumb odjava
2. Študent je odjavljen iz sistema in izgubi možnost uporabe funkcionalnosti, ki so bile na voljo njegovi uporabniški vlogi.

Osnovni tok za uporabniško vlogo Profesor:

1. Profesor klikne na gumb odjava
2. Profesor je odjavljen iz sistema in izgubi možnost uporabe funkcionalnosti, ki so bile na voljo njegovi uporabniški vlogi.

Osnovni tok za uporabniško vlogo Administrator:

1. Administrator klikne na gumb odjava
2. Administrator je odjavljen iz sistema in izgubi možnost uporabe funkcionalnosti, ki so bile na voljo njegovi uporabniški vlogi.


#### Alternativni tok
* Ni alternativnih tokov

#### Izjemni tokovi
* Ni izjemnih tokov. V primeru, da sistem ne dela se bo uporabniku prikazalo obvestilo.

#### Pogoji
* Funkcionalnost Odjava je namenjena uporabniškim vlogam Študent, Profesor in Administrator, ki so prijavljeni v sistem. Uporabniški vlogi Neregistriran uporabnik funkcionalnost ni na voljo.

#### Posledice
* Ko kliknemo na gumb Odjava, smo odjavljeni iz sistema in se vrnemo na začetno stran vloge Neregistriranega uporabnika. 

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* Must have

#### Sprejemni testi 

1. Testiranje funkcionalnosti odjava kot študent:
	* uporabnik je registriran kot študent na poljubnem zaslonu
	* izberemo gumb odjava.
	* izgubimo privilegije svoje vloge
2. Testiranje funkcionalnosti odjava kot profesor:
	* uporabnik je registriran kot profesor na poljubnem zaslonu.
	* izberemo gumb odjava.
	* izgubimo privilegije svoje vloge
3. Testiranje funkcionalnosti odjava kot administrator:
	* uporabnik je registriran kot administrator
	* izberemo gumb odjava.
	* izgubimo privilegije svoje vloge


------------------

###12. Dodaj predmet v redovalnico

#### Povzetek funkcionalnosti
* Študent lahko doda predmet v redovalnico. Pri tem izpolne potrebna polja

#### Osnovni tok

Osnovni tok za dodaj predmet v redovalnico kot študent

1.  Študent klikne na gumb Redovalnica, prikaže se redovalnica študenta
2.	Študent klikne na gumb za dodat predmet v redovalnico
3.	Odpre se uporabniški vmesnik kjer lahko doda predmet.
4.	Študent določi ime predmeta, ciljno oceno ter končno oceno
5.  Študent klikne na gumb Dodaj
6.	Na novo dodan predmet je prikazan v redovalnici z vsemi podatki.

#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Če študent ne izpolni zaželjena polja mu sistem javi napako in predmet ni shranjen.

#### Pogoji
* Funkcionalnost Odjava je namenjena uporabniški vlogi Študent. Uporabniškim vlogam Neregistriran uporabnik, Administrator in Profesor funkcionalnost ni na voljo.
* Študent mora imeti dodan vsaj 1 predmet v redovalnici, ki ga lahko uredi.

#### Posledice
* Če se predmet uspešno doda, se posodobljena redovalnica shrani v bazo, študentu pa se posodobi redovalnica na strani.

#### Posebnosti
* Ni posebnosti.

#### Prioritete identificiranih funkcionalnosti
* Should have


#### Sprejemni testi 

1. Testiranje funkcionalnosti dodaj predmet v redovalnico kot študent:
	* uporabnik je registriran kot študent na zaslonu ki prikazuje redovalnico
	* izberemo gumb dodaj predmet v redovalnico kjer izpolnemo vsa potrebna polja ter potrdimo s klikom na gumb dodaj.
	* v redovalnico je dodan predmet s ciljno in končno oceno

-------------

###13. Uredi/izbriši predmet iz redovalnice

#### Povzetek funkcionalnosti
* Študent lahko uredi/izbriše predmet v redovalnici

#### Osnovni tok

Osnovni tok za uredi/izbriši predmet iz redovalnice kot študent

1.  Študent klikne na gumb Redovalnica, prikaže se redovalnica študenta
2.	Študent klikne na gumb Uredi
3.	Odpre se uporabniški vmesnik kjer lahko uredi predmet.
4.	Študent lahko spremeni ime predmeta, ciljno oceno ter končno oceno. S klikom na gumb Uredi zaključi postopek. Če namesto tega klikne gumb Izbriši in dodatno potrdi, se predmet izbriše iz redovalnice.
5.	Na novo urejen predmet je prikazan v redovalnici z vsemi spremenjenimi podatki. Če je bil predmet izbrisan, ne bo priakzan v redovalnici.  


#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Če študent ne izpolni zaželjena polja mu sistem javi napako in predmet ni shranjen.

#### Pogoji
* Funkcionalnost Odjava je namenjena uporabniški vlogi Študent. Uporabniškim vlogam Neregistriran uporabnik, Administrator in Profesor funkcionalnost ni na voljo.
* Študent mora imeti dodan vsaj 1 predmet v redovalnici, ki ga lahko uredi.

#### Posledice
* Če se predmet uspešno uredi ali izbriše, se posodobljena redovalnica shrani v bazo, študentu pa se posodobi redovalnica na strani.


#### Posebnosti
* Ni posebnosti.


#### Prioritete identificiranih funkcionalnosti
* Should have


#### Sprejemni testi 

1. Testiranje urejanja/brisanja predmeta iz redovalnice
	* Uporabnik se nahaja na strani z redovalnico, prijavljen kot študent
	* Uporabnik spremeni predmet v redovalnici
	* redovalnica se je pravilno posodobila
	
2. Testiranje brisanja predmeta iz redovalnice
	* Uporabnik se nahaja na strani z redovalnico, prijavljen kot študent
	* Uporabnik izbriše predmet iz redovalnice
	* Predmet se je odstranil iz redovalnice

-------------


###14. Vpiši me v predmet

#### Povzetek funkcionalnosti

* Študent se lahko vpiše v izbrani predmet preko uporabniškega vmesnika. vmesnik mu še dodatno omogoča izbiro prenosa "Obveznosti predmeta" in ali "čas izvajanja predmeta".

#### Osnovni tok

Osnovni tok za uporabniško vlogo študenta, ki se želi vpisati v predmet:

1. Študent izbere gumb za predmete.
2. Sistem prikaže Fakultete.
3. Študent izbere fakulteto.
4. Sistem prikaže letnike.
5. Študent izbere letnik
6. Sistem prikaže predmete za izbrano fakulteto in letnik
7. Študent klikne na gumb Vpiši me zraven predemta
8. Sistem prikaže izbiro(checkbox) kaj želi prenesti na svoj urnik. Na izbiro ima Urnik in/ali Obveznosti(kolokviji/izpiti/naloge).
9. Študent izbere željene nastavitve in potrdi izbiro.
                        
#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi
* Ni izjemnih tokov

#### Pogoji

* Funkcionalnost Vpiši me v predmet je na voljo uporabniški vlogi Študent. Uporabniškim vlogam Neregistriran uporabnik, Profesor in Administrator funkcionalnost ni na voljo.

#### Posledice

* Študentu se ob vpisu na predmet prenesejo obveznosti in/ali čas izvajanja predmeta v urnik in/ali koledar.

#### Posebnosti

* Potrebno je implementirati Google Calendar API, za shranjevanje dogodkov v koledar.

#### Prioritete identificiranih funkcionalnosti

* Must have

#### Sprejemni testi

1. Testiranje vpisa v predmet
	* Uporabnik se nahaja na strani s predmeti, prijavljen kot študent
	* Uporabnik poišče ustrezen predmet in se na njega vpiše
	* Uporabniku se dodajo zaželjene opcije v urnik in/ali koledar

-------------

###15. Potrditev prošnje

#### Povzetek funkcionalnosti

* Administrator lahko pregleda prošnje profesorjev, ki želijo odobritev pravic ter potrdi legitimne. 

#### Osnovni tok

1. Administrator pregleda prošnje profesorjev, ki jih ima na voljo
2. Izbere prošnjo, ki jo želi podrobneje pregledati
3. Administrator preveri legitimnost profesorja
4. Administrator klikne na gumb Potrdi in s tem omogoči Profesorju uporabo aplikacije
                        
#### Alternativni tok(ovi)
* Ni alternativnih tokov

#### Izjemni tokovi

* Administrator pozabi geslo in nima več dostopa do sistema.

#### Pogoji

* Funkcionalnost Potrditev prošnje je na voljo uporabniški vlogi Administrator. Uporabniškim vlogam Neregistriran uporabnik, Profesor in Študent funkcionalnost ni na voljo.

#### Posledice

* Profesorju je omogočen dostop uporabe aplikacije 

#### Posebnosti

* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti

* Must have

#### Sprejemni testi
1.	Testiranje potrditve prošnje
	* Nahajamo se na strani za potrjevanje profesorjev, prijavljeni kot Administrator
	* Uporabnik izbere prošnjo profesorja in potrdi prošnjo profesorja.
	* profesor ima sedaj pravico ustvarjati predmete

-------------

###16. Mesečni pogled koledarja

#### Povzetek funkcionalnosti

* Študent lahko koledarja prikaže v obliki mesečnega pogleda.

#### Osnovni tok

1. Študent klikne na gumb koledarja
2. Prikaže se mu zaslon tedenskega koledarja, kjer lahko dodaja in ureja predmete
3. Študent klikne na gumb Mesečni pogled
4. Prikaže se mu zaslon mesečnega koledarja, ki omogoča širši prikaz obveznosti

#### Alternativni tok
* Ni alternativnih tokov

#### Izjemni tokovi
* Ni izjemnih tokov. V primeru, da sistem ne dela se bo uporabniku prikazalo obvestilo.

#### Pogoji
* Funkcionalnost Mesečni pogled je namenjena uporabniški vlogi Študent. Uporabniškim vlogam Neregistriran uporabnik, Profesor in Administrator funkcionalnost ni na voljo.

#### Posledice
* Ko kliknemo na gumb Mesečni pogled, se nam prikaže zaslon mesečnega koledarja

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* Would have

#### Sprejemni testi 
1. Testiranje mesečnega pogleda koledarja
	* Uporabnik se nahaja na glavni strani prijavljen kot študent
	* Uporabnik klikne na gumb Koledar, prikaže se mu tedenska oblika koledarja. 
	* Uporabnik klikne na gumb Mesečni pogled, prikaže se mu mesečna oblika koledarja.

------------------

###17. Izbira datuma na koledarju

#### Povzetek funkcionalnosti

* Študent lahko izbere datum prikaza na koledarju. Ko izbere določen dan, se mu prikaže koledar v tedenski obliki od določenega datuma naprej.

#### Osnovni tok

1. Študent klikne na gumb za prikaz koledarja
2. Prikaže se mu koledar. 
3. Nato klikne na gumb za izbiro datuma, kjer se mu prikaže mesečni koledar, v katerem se lahko premika po mesecih
4. Ko klikne na želen datum, se mu izbran datum prikaže v osnovnem koledarju v tedenski obliki od izbranega datuma naprej

#### Alternativni tok
* Ni alternativnih tokov

#### Izjemni tokovi
* Ni izjemnih tokov. V primeru, da sistem ne dela se bo uporabniku prikazalo obvestilo.

#### Pogoji
* Funkcionalnost Izbira datuma na koledarju je namenjena uporabniški vlogi Študent. Uporabniškim vlogam Neregistriran uporabnik, Profesor in Administrator funkcionalnost ni na voljo.

#### Posledice
* Ob izbiri datuma, se nam ta prikaže v tedenskem urniku

#### Posebnosti
* Ni posebnosti

#### Prioritete identificiranih funkcionalnosti
* Could have

#### Sprejemni testi 

1. Testiranje izbire datuma na koledarju
	* Uporabnik se nahaja na glavni strani prijavljen kot študent
	* Uporabnik klikne na gumb Koledar, prikaže se mi tedenska oblika koledarja. 
	* Uporabnik klikne na gumb za izbiro datuma. Prikaže se mu koledar, kjer izbere poljuben datum.
	* Datum se mu prikaže v tedenskem koledarju.

---------------

## 6. Nefunkcionalne zahteve
* zahteva izdelka: 
    * Sistem mora biti dosegljiv na javno dostopnem spletnem naslovu
    * Sistem mora biti na voljo najmanj 90 odstotkov časa v letu
    * Sistem mora biti zmožen streči najmanj 10.000 hkratnim uporabnikom.
    * sistem mora na vsako poizvedbo odgovoriti v največ 700 milisekundah
	* Sistem mora zasedati manj kot 10 GB prostora.
	* Sistem mora biti odporen proti DDos napadom.

* Organizacijske zahteve:
    * Profesorji ki uporabljaji sistema se morajo identificirat pri Administratorju.
	* Dostopa do strežnikov, ker se nahaja sistem ne smejo imeti osebe ki niso za to pooblaščene	
	* Razvoj sistema ne sme trajati več kot 11 mesecev.
	
* Zunanje zahteve:
    * Vsa poročila, ki jih sistem izdela, morajo biti skladna s standardom GPDR
    * Sistem uporabniku ne sme omogočiti dostopa do podatkov, za katere ni izrecno pooblaščen.



## 7. Prototipi vmesnikov

####Zaslonske maske in reference na primere uporabe:
1. **Izbris.PNG:**
	* prikazuje obvestilo, ki se prikaže vsakič, ko želimo izbrisat določeno stvar
2. **Koledar.PNG:**
	* prikazuje primer uporabe 3., 4. in 17. funkcionalnosi. 
	* Zaslonska maska prikazuje koledar, 
	* gumb za dodajanje obveznosti
	* gumb za izbiro datuma
	* gumb za prehod na mesečni koledar
	* prikazno okno za ustvarjat obveznost
	* prikazno okno za urejat/izbrisat obveznost
3. **Mesecni_pogled.PNG:**
	* prikazuje primer uporabe 16. funkcionalnosti
	* prikazuje mesečni koledar z obveznostmi
	* gumb za prehod na tedenski
	* gumb za dodajanje obveznosti
	* gumb za izbiro datuma 
4. **Prenesi.PNG:**
	* prikazuje primer uporabe 14. funkcionalnosti
	* prikazuje seznam predmetov na katere se lahko študent vpiše
	* podrobnejši opis predmeta
	* gumb za vpis v predmet
	* prikazno okno za izbiro sinhronizacije urnika in/ali koledarja
5. **Prijava.PNG:**
	* prikazuje primer uporabe 2. funkcionalnosti
	* polja za vpis maila naslova in gesla
	* gumb za prijavo
6. **Profesor.PNG:**
	* prikazuje primer uporabe 5. in 6. funkcionalnosti
	* prikazuje seznam predmetov profesorja
	* gumb za dodajanje predmetov
	* pojavno okno za dodajanje predmeta
	* pojavno okno za urejanje/brisanje predmeta
7. **Prosnje.PNG:**
	* prikazuje primer uporabe 15. funkcionalnosti
	* seznam prošenj na novo registriranih profesorjev
	* gumb za potrditev prošnje
	* gumb za zavrnitev pršnje
8. **Redovalnica.PNG:**
	* prikazuje primer uporabe 11., 12. in 13. funkcionalnosti
	* seznam predmetov in njihovih ocen
	* gumb za urejanje predmeta
	* gumb za dodajanje predmeta
9. **Registracija.PNG:**
	* prikazuje primer uporabe 1. funkcionalnosti
	* polja za ime, priimek, fakulteto, datum rojstva, spol, mail in geslo
	* gumb za registracijo študenta 
	* gumb za registracijo profesorja
10. **Todo.PNG:**
	* prikazuje primer uporabe 7. in 8. funkcionalnosti
	* seznam z opravili
	* gumb za dodajanje opravila
	* gumb za dokončanje opravila
11. **Urnik_aktivnosti.PNG:**
	* prikazuje primer uporabe 9. in 10. funkcionalnosti
	* urnik z aktivnostmi
	* gumb za dodajanje aktivnosti
	* prikazno okno za urejanje/brisanje aktivnosti, ki se prikaže ob kliku na aktivnost

####Vmesniki do zunanjih sistemov:

Ko študent doda novo obveznost se v našem primeru kliče na zunanji sistem(API) za dodajanje novih obveznosti v googlov koledar. Za to uporabljamo metodo events.insert().

Da bo klic metode veljaven mora vsebovati vsaj te parametre:
    
* calendarId (to je gmail uporabnika)
* event (ki vsebuje pričetek in konec dogodka)
	* start date
	* end date

Rezultat te metode je to, da se obveznost doda v študentov koledar.

V primeru sprememb obveznosti na profesorjevi strani, se posodobijo obveznosti v koledarjih vpisanih študentov.
        