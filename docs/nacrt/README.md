# Načrt sistema

| | |
|:---|:---|
| **Naziv projekta** | StraightAs |
| **Člani projektne skupine** | **TO-DO** 1. član, 2. član, 3. član in 4. član |
| **Kraj in datum** | **TO-DO** kraj, datum |



## Povzetek

**TO-DO**

* Povzetek je, kot že vemo, celoten dokument, strnjen v največ 150 besed.



## 1. Načrt arhitekture

**TO-DO**

* Za prikaz uporabite enostavne prikaze, kot so blokovni ali paketni diagrami. Uporabite lahko arhitekturne vzorce.


## 2. Načrt strukture

### 2.1 Razredni diagram

**TO-DO**

* Izdelajte razredni diagram.

### 2.2 Opis razredov

**TO-DO**

* Vsak razred podrobno opišite. Opis posameznega razreda naj ima sledečo strukturo:

#### Ime razreda **TO-DO**

* Koncept iz problemske domene, ki ga razred predstavlja.

#### Atributi

**TO-DO**

* Za vsak atribut navedite:
    * ime atributa,
    * podatkovni tip, če ta ni očiten,
    * pomen, če ta ni samoumeven,
    * zalogo vrednosti, če ta ni neomejena ali očitna.


#### Nesamoumevne metode

**TO-DO**

* Za vsako metodo navedite:
    * ime metode,
    * imena in tipe parametrov,
    * tip rezultata,
    * pomen (če ta ni dovolj očiten iz naziva metode in njenih parametrov).


## 3. Načrt obnašanja

**TO-DO**

* Za izdelavo načrta obnašanja lahko uporabite:
    * diagrame zaporedja,
    * končne avtomate,
    * diagrame aktivnosti,
    * diagrame stanj in
    * psevdokodo.



